package db

import (
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	MySQLDBClient *gorm.DB
)

func InitMySQLDBClient(dsn string) {
	// dsn = "bb814033ad167c:f51844b9@tcp(us-cdbr-east-05.cleardb.net)/heroku_eecd8f50d0a10c4"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("init mysql db client failed: %s", err)
	}
	MySQLDBClient = db
}

func InitMySQLDBClientWithSQLite(dsn string) {
	// dsn = "gorm.db"
	db, err := gorm.Open(sqlite.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("init mysql db client failed: %s", err)
	}
	MySQLDBClient = db
}
