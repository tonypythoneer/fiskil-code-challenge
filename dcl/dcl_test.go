package dcl

import (
	"context"
	"encoding/json"
	"fiskil/db"
	"fiskil/model"
	"testing"

	pb "google.golang.org/genproto/googleapis/pubsub/v1"
)

func TestReceiveMessages(t *testing.T) {
	pclient, sclient, _, cleanup := NewFakePubSub(context.TODO(), t)
	defer cleanup()

	top := mustCreateTopic(context.TODO(), t, pclient, &pb.Topic{Name: "projects/P/topics/T"})
	sub := mustCreateSubscription(context.TODO(), t, sclient, &pb.Subscription{
		Name:               "projects/P/subscriptions/S",
		Topic:              top.Name,
		AckDeadlineSeconds: 10,
	})

	// input & publish
	inputMsgs := []Message{
		{ServiceName: "service_a", Severity: "info"},
		{ServiceName: "service_a", Severity: "info"},
		{ServiceName: "service_b", Severity: "info"},
		{ServiceName: "service_b", Severity: "info"},
		{ServiceName: "service_a", Severity: "warn"},
		{ServiceName: "service_b", Severity: "warn"},
		{ServiceName: "service_b", Severity: "warn"},
		{ServiceName: "service_b", Severity: "warn"},
	}
	pubsubMsgs := make([]*pb.PubsubMessage, len(inputMsgs))
	for i, msg := range inputMsgs {
		bytes, _ := json.Marshal(msg)
		pubsubMsgs[i] = &pb.PubsubMessage{Data: bytes}
	}
	want := publish(t, pclient, top, pubsubMsgs)

	// output & subscribe
	got := pubsubMessages(pullN(context.TODO(), t, len(want), sclient, sub))

	outputMsgs := make([]Message, len(inputMsgs))
	index := 0
	for _, v := range got {
		var m Message
		_ = json.Unmarshal(v.Data, &m)
		outputMsgs[index] = m
		index += 1
	}

	// pass dcl
	ReceiveMessages(outputMsgs)

	// validate
	var count int64
	db.MySQLDBClient.Model(&model.ServiceLogs{}).Count(&count)
	if num := len(inputMsgs); int(count) != num {
		t.Fatalf("it expect to receive %d of messages but it is %d", num, count)
	}

	serviceSeverity := &model.ServiceSeverity{}
	db.MySQLDBClient.Model(serviceSeverity).
		Where("service_name = ? AND severity = ?", "service_a", "info").
		Scan(serviceSeverity)
	if serviceSeverity.Count != 2 {
		t.Fatalf("it expect to receive %d of messages but it is %d", 2, count)
	}
}
