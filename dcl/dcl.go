package dcl

import (
	"fiskil/db"
	"fiskil/model"
	"log"
	"time"

	"gorm.io/gorm"
)

const (
	SEVERITY_DEBUG_LEVEL string = "debug"
	SEVERITY_INFO_LEVEL  string = "info"
	SEVERITY_WARN_LEVEL  string = "warn"
	SEVERITY_ERROR_LEVEL string = "error"
	SEVERITY_FATAL_LEVEL string = "fatal"
)

type Message struct {
	ServiceName string    `json:"service_name" gorm:"column:service_name"`
	Payload     string    `json:"payload" gorm:"column:payload"`
	Severity    string    `json:"severity" gorm:"column:severity"`
	Timestamp   time.Time `json:"timestamp" gorm:"column:timestamp"`
}

func ReceiveMessages(msgs []Message) {
	// group by
	counts := make(map[string]map[string]int) // service_name:log_level:count
	for _, msg := range msgs {
		if _, ok := counts[msg.ServiceName]; !ok {
			counts[msg.ServiceName] = map[string]int{
				SEVERITY_DEBUG_LEVEL: 0,
				SEVERITY_INFO_LEVEL:  0,
				SEVERITY_WARN_LEVEL:  0,
				SEVERITY_ERROR_LEVEL: 0,
				SEVERITY_FATAL_LEVEL: 0,
			}
		}

		switch msg.Severity {
		case SEVERITY_DEBUG_LEVEL:
			counts[msg.ServiceName][SEVERITY_DEBUG_LEVEL] += 1
		case SEVERITY_INFO_LEVEL:
			counts[msg.ServiceName][SEVERITY_INFO_LEVEL] += 1
		case SEVERITY_WARN_LEVEL:
			counts[msg.ServiceName][SEVERITY_WARN_LEVEL] += 1
		case SEVERITY_ERROR_LEVEL:
			counts[msg.ServiceName][SEVERITY_ERROR_LEVEL] += 1
		case SEVERITY_FATAL_LEVEL:
			counts[msg.ServiceName][SEVERITY_FATAL_LEVEL] += 1
		}
	}

	err := db.MySQLDBClient.Transaction(func(tx *gorm.DB) error {
		// do some database operations in the transaction (use 'tx' from this point, not 'db')

		if err := tx.Table(model.ServiceLogsTable).Create(msgs).Error; err != nil {
			// return any error will rollback
			return err
		}

		for serviceName, severityCounts := range counts {
			for severity, count := range severityCounts {
				if count == 0 {
					continue
				}

				if err := tx.Create(&model.ServiceSeverity{
					ServiceName: serviceName,
					Severity:    severity,
					Count:       count,
				}).Error; err != nil {
					return err
				}
			}
		}

		// return nil will commit the whole transaction
		return nil
	})
	if err != nil {
		log.Printf("failed to save log: %s", err)
	}
}

func Test() {
	msgs := []Message{
		{ServiceName: "service_a", Severity: "info"},
		{ServiceName: "service_a", Severity: "info"},
		{ServiceName: "service_b", Severity: "info"},
		{ServiceName: "service_b", Severity: "info"},
		{ServiceName: "service_a", Severity: "warn"},
		{ServiceName: "service_b", Severity: "warn"},
		{ServiceName: "service_b", Severity: "warn"},
		{ServiceName: "service_b", Severity: "warn"},
	}
	ReceiveMessages(msgs)
}
