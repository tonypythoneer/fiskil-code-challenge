package dcl

import (
	"context"
	"fiskil/db"
	"os"
	"sync/atomic"
	"testing"
	"time"

	"cloud.google.com/go/pubsub/pstest"

	pb "google.golang.org/genproto/googleapis/pubsub/v1"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestMain(m *testing.M) {
	////////////////////////////////////////
	// before
	////////////////////////////////////////
	dsn := "test.db"
	db.InitMySQLDBClientWithSQLite(dsn)
	db, _ := db.MySQLDBClient.DB()
	db.Exec(`
	CREATE TABLE service_logs (
		service_name VARCHAR(100) NOT NULL,
		payload VARCHAR(2048) NOT NULL,
		severity VARCHAR(5) NOT NULL,
		timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
	)
	`)
	db.Exec(`
	CREATE TABLE service_severity (
		service_name VARCHAR(100) NOT NULL,
		severity VARCHAR(5) NOT NULL,
		count INT(4) NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
	);
	`)

	////////////////////////////////////////
	// main
	////////////////////////////////////////
	exitVal := m.Run()

	////////////////////////////////////////
	// after
	////////////////////////////////////////
	db.Close()
	_ = os.Remove(dsn)

	os.Exit(exitVal)
}

////////////////////////////////////////
// pstest
////////////////////////////////////////

func NewFakePubSub(ctx context.Context, t *testing.T, opts ...pstest.ServerReactorOption) (pb.PublisherClient, pb.SubscriberClient, *pstest.Server, func()) {
	srv := pstest.NewServer(opts...)
	conn, err := grpc.DialContext(ctx, srv.Addr, grpc.WithInsecure())
	if err != nil {
		t.Fatal(err)
	}
	return pb.NewPublisherClient(conn), pb.NewSubscriberClient(conn), srv, func() {
		srv.Close()
		conn.Close()
	}
}

func mustCreateTopic(ctx context.Context, t *testing.T, pc pb.PublisherClient, topic *pb.Topic) *pb.Topic {
	top, err := pc.CreateTopic(ctx, topic)
	if err != nil {
		t.Fatal(err)
	}
	return top
}

func pubsubMessages(rms map[string]*pb.ReceivedMessage) map[string]*pb.PubsubMessage {
	ms := map[string]*pb.PubsubMessage{}
	for k, rm := range rms {
		ms[k] = rm.Message
	}
	return ms
}

func mustCreateSubscription(ctx context.Context, t *testing.T, sc pb.SubscriberClient, sub *pb.Subscription) *pb.Subscription {
	sub, err := sc.CreateSubscription(ctx, sub)
	if err != nil {
		t.Fatal(err)
	}
	return sub
}

func pullN(ctx context.Context, t *testing.T, n int, sc pb.SubscriberClient, sub *pb.Subscription) map[string]*pb.ReceivedMessage {
	got := map[string]*pb.ReceivedMessage{}
	for i := 0; len(got) < n; i++ {
		res, err := sc.Pull(ctx, &pb.PullRequest{Subscription: sub.Name, MaxMessages: int32(n - len(got))})
		if err != nil {
			t.Fatal(err)
		}
		for _, m := range res.ReceivedMessages {
			got[m.Message.MessageId] = m
		}
	}
	return got
}

var now atomic.Value

func publish(t *testing.T, pclient pb.PublisherClient, topic *pb.Topic, messages []*pb.PubsubMessage) map[string]*pb.PubsubMessage {
	pubTime := time.Now()
	now.Store(func() time.Time { return pubTime })
	defer func() { now.Store(time.Now) }()

	res, err := pclient.Publish(context.Background(), &pb.PublishRequest{
		Topic:    topic.Name,
		Messages: messages,
	})
	if err != nil {
		t.Fatal(err)
	}
	tsPubTime := timestamppb.New(pubTime)
	want := map[string]*pb.PubsubMessage{}
	for i, id := range res.MessageIds {
		want[id] = &pb.PubsubMessage{
			Data:        messages[i].Data,
			Attributes:  messages[i].Attributes,
			MessageId:   id,
			PublishTime: tsPubTime,
		}
	}
	return want
}
