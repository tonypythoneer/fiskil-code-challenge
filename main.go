package main

import (
	"fiskil/db"
	"fiskil/dcl"
	"flag"
	"fmt"
)

func init() {
	var userFlag string
	var pwdFlag string
	var hostFlag string
	var dbFlag string
	flag.StringVar(&userFlag, "user", "", "")
	flag.StringVar(&pwdFlag, "pwd", "", "")
	flag.StringVar(&hostFlag, "host", "", "")
	flag.StringVar(&dbFlag, "db", "", "")
	flag.Parse()

	// "bb814033ad167c:f51844b9@tcp(us-cdbr-east-05.cleardb.net)/heroku_eecd8f50d0a10c4"
	db.InitMySQLDBClient(fmt.Sprintf("%s:%s@tcp(%s)/%s", userFlag, pwdFlag, hostFlag, dbFlag))
}

func main() {
	dcl.Test()
}
