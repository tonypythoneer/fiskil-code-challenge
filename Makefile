################################################################################
## run code                                                                   ##
################################################################################

# make test
.PHONY: run
run:
	go run ./... \
		--user=bb814033ad167c \
		--pwd=f51844b9 \
		--host=us-cdbr-east-05.cleardb.net \
		--db=heroku_eecd8f50d0a10c4

################################################################################
## run test                                                                   ##
################################################################################

# make test
.PHONY: test
test:
	go test ./... -v

################################################################################
## run coverage and generate report                                           ##
################################################################################

# make coverage
.PHONY: coverage
coverage:
	go test ./... -v -coverprofile=cover.out
	go tool cover -html=cover.out -o cover.html

################################################################################
## release and minimize the binary size                                       ##
################################################################################

.PHONY: release
release:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -ldflags="-s -w \
		--extldflags "-static""
