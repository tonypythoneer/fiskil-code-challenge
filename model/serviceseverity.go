package model

import "time"

/*
CREATE TABLE service_severity (
	service_name VARCHAR(100) NOT NULL,
	severity ENUM("debug", "info", "warn", "error", "fatal") NOT NULL,
	count INT(4) NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
*/

type ServiceSeverity struct {
	ServiceName string    `gorm:"column:service_name"`
	Severity    string    `gorm:"column:severity"`
	Count       int       `gorm:"column:count"`
	CreatedAt   time.Time `gorm:"column:created_at"`
}

func (ServiceSeverity) TableName() string {
	return "service_severity"
}
