package model

import "time"

const (
	ServiceLogsTable string = "service_logs"
)

/*
CREATE TABLE service_logs (
	service_name VARCHAR(100) NOT NULL,
	payload VARCHAR(2048) NOT NULL,
	severity ENUM("debug", "info", "warn", "error", "fatal") NOT NULL,
	timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
*/
type ServiceLogs struct {
	ServiceName string    `gorm:"column:service_name"`
	Payload     string    `gorm:"column:payload"`
	Severity    string    `gorm:"column:severity"`
	Timestamp   time.Time `gorm:"column:timestamp"`
	CreatedAt   time.Time `gorm:"column:created_at"`
}

func (ServiceLogs) TableName() string {
	return ServiceLogsTable
}
