# fiskil-code-challenge

There is a logic code for DCL in `dcl/dcl.go`

It's just a function and only receive one argument, `[]Message`.

It will group by `service_name` and `severity` to count its quantity

Finally, it will save into two tables which are `service_logs` and `service_severity`.

## project structure

```txt
.
├── db
│   └── gorm.go - using orm lib to operate db
├── dcl
│   ├── dcl.go - main logic for dealing with data
│   ├── dcl_main_test.go - some test utils
│   └── dcl_test.go - to test it in a pub sub environment
├── go.mod - the dependency list for this code base
├── go.sum
├── main.go - the entry code for this whole codebase
├── Makefile - a shortcut command list
├── model
│   ├── servicelog.go
│   └── serviceseverity.go
└── README.md - The doc for this codebase/
```

## How to use

Run command

```sh
make run
```

Or, you can use your parameters:

```XXX
go run ./... \
    --user=XXX \
    --pwd=XXX \
    --host=XXX \
    --db=XXX
```

## test

You can run `make test` to get the result.

The package, `fiskil/dcl`, is using `pstest` and `sqlite` to test its scenario.

About `pstest`, it will build `publish client` and `subscrition client` for the environment. And, it will pull data from `subscrition client` and let `dcl function` deal with it.

About `sqlite`, it just simply the `dockertest` because it can build local database file and validate the logic workflow when we don't want to build a complex environment for unit tests. 

## coverage

You can run `make coverage` to get the result

```txt
coverage: 66.7% of statements
ok      fiskil/dcl      0.091s  coverage: 66.7% of statements
?       fiskil/model    [no test files]
```

## CI

It's using Gitlab CI

When you trigger `any branch or tag`, it will run `test` stage and run unit tests to validate the codebase.

When you push new `tag`, it will run `release` stage and generate `binary code` in Gitlab's assets.

## assets

You can check this project's CI page to get the binary code: https://gitlab.com/tonypythoneer/fiskil-code-challenge/-/jobs

* When you are `darwin` & `amd64` -  please use `assets/assessment_darwin_amd64`
* When you are `darwin` & `arm64` -  please use `assets/assessment_darwin_arm64`
* When you are `linux` & `amd64` - please use ` assets/assessment_linux_amd64`
* When you are `windows` & `amd64` -   please use `ssets/assessment_windows_amd64`
